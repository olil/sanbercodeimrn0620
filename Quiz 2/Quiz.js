
console.log('Quiz 2')
console.log('==============================================')
console.log('No. 1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)')
console.log('==============================================')

class Score {
    constructor(points = 1){

      this.subject = 'score';
      this.points = points;
      this.email = 'olil@gmail.com';

    }
    
    average(){
      const { points } = this

      if (points.length > 1) {
        let values = points.reduce((previous, current) => current += previous);

        return values /= points.length;
      }
      return points
    }
  
  }
  
  var score = new Score([1,2,3])
  console.log(score.average())

console.log('===============================================')
console.log('No. 2. SOAL Create Score (10 Poin + 5 Poin ES6)')
console.log('===============================================')

const data = [
    
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]

  ]
   var viewScores = (data, subject) => {
    let x = data[0].indexOf(subject)
    let arr = []

    for(let y = 1; y < data.length; y++){
      let object = {
        email : data[y][0],
        subject,
        points: data[y][x]
      }
      arr.push(object)
    }
    console.log(arr)
  }
  
  // TEST CASE
  viewScores(data, "quiz-1")
  viewScores(data, "quiz-2")
  viewScores(data, "quiz-3")

console.log('===============================================')
console.log('No. 3. SOAL Recap Score (15 Poin + 5 Poin ES6)')
console.log('===============================================')

var recapScores = (data) => {
    let arr = [];
    for(let x = 1; x < data.length; x++){
      let y = (data[x].splice(1))
      arr.push(y)

      let values = y.reduce((previous, current) => current += previous);
      let nilai = (values /= y.length).toFixed(1)
      var predikat = "";
      if(nilai >= 70) {
        predikat = "participant"
      } else if(nilai >= 80 ) {
          predikat = "graduate" 
      }
      else if(nilai >= 90 ) {
          predikat = "honour"
      }
      else {
          predikat = "tidak lulus";
      }
          
      console.log(`${x}. email: ${data[x][0]} \n Rata-Rata: ${nilai} \n Predikat: ${predikat} \n`)
    }
  }
  
  recapScores(data);
  