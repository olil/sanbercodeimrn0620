console.log('Tugas 4 – Functions')
console.log(' ')
console.log('No. 1')
console.log('======================')

function teriak() {
    console.log("Halo Sanbers!");
  }
   
  teriak();

/////////////////////////////////////////////////
console.log(' ')
console.log('No. 2')
console.log('======================')

function kalikan(num1, num2){
    return num1 * num2
}

var hasilKali = kalikan(12, 4)
console.log(hasilKali)

/////////////////////////////////////////////////
console.log(' ')
console.log('No. 3')
console.log('======================')


function introduce(name, age, address, hobby){
    var name = "Agus"
    var age = 30
    var address = "Jln. Malioboro, Yogyakarta"
    var hobby = "Gaming"
    console.log('Nama saya '+name+', umur saya '+age+' tahun'+', alamat saya di '+address+' , dan saya punya hobby yaitu '+hobby)
}
introduce()