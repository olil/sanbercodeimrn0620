console.log('Tugas 3')
console.log(' ')
console.log('No. 1 Looping While')
console.log('===========Looping Pertama===========')

var deret = 2;
while(deret <= 20) {
  console.log(deret + ' - I love coding')
  deret+=2;
}
 
console.log('================Looping Ke Dua===============');

while(deret > 2) { 
    deret-=3;
    deret ++;
    console.log(deret + ' - I will become a mobile developer')
  }


console.log(' ')
console.log('No. 2 Looping menggunakan for')
console.log('==================================')

for (var x=1; x<=20; x++) {
    if (x % 3 === 1) {
            console.log(x +  " - Santai");
    }
    else if (x % 2 === 0) {
            console.log(x + " - Berkualitas");   
    }else { if(x%3 == 0 && x%1 == 0){
                console.log(x + ' - I love coding')
                
        }
    }
}


console.log(' ')
console.log('No. 3 Membuat Persegi Panjangr')
console.log('==================================')

var pp = "#"
var baris = 4
var kolom = 8
for (var x = 0; x < baris; x++){
        pp=''
        for(y = 0; y < kolom ; y++){ 
            pp = pp+'#'      
        }
        
        console.log(pp)
}


console.log(' ')
console.log('No. 4 Membuat Tangga')
console.log('==================================')

var ss = ""
var angka1 = 1
var angka2 = 2
for (angka1=1; angka1 <= 7; angka1+=1){
        for(angka2=1; angka2 <=angka1 ; angka2+=1){     
        }
        ss += "#"
        console.log(ss)
}


console.log(' ')
console.log('No. 5 Membuat Papan Catur')
console.log('==================================')

var board  = "";
var rows = 0;

while(rows < 8) {
	
	var cols = 0;
	var chessBoard;
	if(rows % 2 === 0 ) {
		chessBoard = true
	} else {
		chessBoard = false;
	}
	while(cols < 8) {
		
		if(chessBoard) {
			board += ' ';
		} else {
			board += '#';
		}
		chessBoard = !chessBoard;
		cols++;
	}
	board += "\n";
	rows++;
}
console.log(board);