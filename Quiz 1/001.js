console.log(' ')
console.log('A. Bandingkan Angka')
console.log('==================================')

function bandingkan(number1 = 0, number2 =0) {
  // Tulis code kamu di sini
    if(number1 && number2 !== undefined){

      if(number1 < 0 || number2 < 0 || number1 === number2) 
        return -1
      else{

        if(number1 > number2) 
          return number1
        else 
          return number2
      }
    }

    else 
      return -1
  }

    console.log(bandingkan(10, 15)); // 15
    console.log(bandingkan(12, 12)); // -1
    console.log(bandingkan(-1, 10)); // -1 
    console.log(bandingkan(112, 121));// 121
    console.log(bandingkan(1)); // 1
    console.log(bandingkan()); // -1
    console.log(bandingkan("15", "18")) // 18


console.log(' ')
console.log('B. Balik String')
console.log('==================================')

function balikString(string) {
  // Tulis code kamu di sini
    var kata= ""
    for(let i = string.length; i >= 0 ; i--){
      kata += string.charAt(i)
    }
    return kata
  }

    console.log(balikString("abcde")) // edcba
    console.log(balikString("rusak")) // kasur
    console.log(balikString("racecar")) // racecar
    console.log(balikString("haji")) // ijah


console.log(' ')
console.log('C. Palindrome')
console.log('==================================')

function palindrome(string) {
  // Tulis code kamu di sini
    var kataBaru= ""
    for(let i = string.length; i >= 0 ; i--){
      kataBaru += string.charAt(i)
    }

    if(string === kataBaru) 
      return true
    else 
      return false
  }

    console.log(palindrome("kasur rusak")) // true
    console.log(palindrome("haji ijah")) // true
    console.log(palindrome("nabasan")) // false
    console.log(palindrome("nababan")) // true
    console.log(palindrome("jakarta")) // false